# Bellis

Bellis is a powerful indexing system. Its primary goal is to maintain an up-to-date list of files, possibly annotated. Maintaining up-to-date may rely on original data source (typically the file system: when you add a file in the file system, Bellis add a line in the index), but can evolve from another index (remote index, RSS feed, ...).

It is based on a modular system that can work on file system, file or web content, ... and offers multiple storage formats, such as database, XML file, ...

## Usage

TODO

## Documentation

Have a look to the wiki at https://github.com/nemunaire/bellis/wiki

## Building

TODO

## License

This is free software; you can redistribute it and/or modify it under the same terms as GNU General Public Licence v3.
